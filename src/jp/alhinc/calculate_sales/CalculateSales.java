package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String NUMBER_OF_INVALID ="合計金額が10桁を超えました";
	private static final String BRANCH_CODE_IS_INVALID ="の支店コードが不正です";
	private static final String FORMAT_IS_INVALID ="のフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBERS ="売上ファイル名が連番になっていません";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数エラーチェック
		if(args.length !=1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File>rcdFiles =new ArrayList<>();

		for(int i =0; i<files.length; i++) {
			String fileName = files[i].getName();
			//ファイルかどうか確認する
			//0～9を含む8桁の数字で始まり".rcd"で終わるファイル名を判定する
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//連番エラーチェック
		for(int i = 0; i < rcdFiles.size() -1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));

			if((latter - former) !=1) {
				System.out.println(FILE_NOT_SERIAL_NUMBERS);
				return;
			}
		}

		for(int i = 0;i < rcdFiles.size();i++) {
			BufferedReader br = null;

			try {
				File file = new File(args[0], rcdFiles.get(i).getName());
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);


				List<String> list = new ArrayList<>();
				String line = null;
				while((line = br.readLine()) != null) {

					list.add(line);

				}//売上ファイルエラーチェック
				if(list.size() !=2) {
					System.out.println(FORMAT_IS_INVALID);
					return;
				}

				//ファイルの1行目にある支店コードをファイルから読み込む
				String branchCode = list.get(0);
				//keyエラーチェック
				if(!branchSales.containsKey(branchCode)) {
					System.out.println(BRANCH_CODE_IS_INVALID);
					return;
				}
				Long filesSale = Long.parseLong(list.get(1));
				//数字確認
				if(!list.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				Long saleAmont = branchSales.get(branchCode) + filesSale;
				//オーバーエラーチェック
				if(saleAmont >= 10000000000L) {
					System.out.println(NUMBER_OF_INVALID);
				}
				branchSales.replace(branchCode, saleAmont);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return ;
					}
				}
			}
		}


		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
	}


/**
 * 支店定義ファイル読み込み処理
 *
 * @param フォルダパス
 * @param ファイル名
 * @param 支店コードと支店名を保持するMap
 * @param 支店コードと売上金額を保持するMap
 * @return 読み込み可否
 */

private static boolean readFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
	BufferedReader br = null;

	try {
		File file = new File(path, fileName);
		// ファイルエラーチェック
		if(!file.exists()) {
			System.out.println(FILE_NOT_EXIST);
			return false;
		}
		FileReader fr = new FileReader(file);
		br = new BufferedReader(fr);

		String line;
		// 一行ずつ読み込む
		while((line = br.readLine()) != null) {

			// ※ここの読み込み処理を変更してください。(処理内容1-2)
			String[] items = line.split(",");

			//フォーマットエラーチェック
			if((items.length !=2) ||(!items[0].matches("^[0-9]{3}"))){
				System.out.println(FILE_INVALID_FORMAT);
				return false;
			}
			branchNames.put(items[0],items[1]);
			branchSales.put(items[0],0L);

			System.out.println(line);
		}

	} catch(IOException e) {
		System.out.println(UNKNOWN_ERROR);
		return false;
	} finally {
		// ファイルを開いている場合
		if(br != null) {
			try {
				// ファイルを閉じる
				br.close();
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			}
		}
	}
	return true;
}

/**
 * 支店別集計ファイル書き込み処理
 *
 * @param フォルダパス
 * @param ファイル名
 * @param 支店コードと支店名を保持するMap
 * @param 支店コードと売上金額を保持するMap
 * @return 書き込み可否
 */
private static boolean writeFile(String path, String fileName, Map<String, String> branchNames, Map<String, Long> branchSales) {
	// ※ここに書き込み処理を作成してください。(処理内容3-1)
	BufferedWriter bw = null;

	try {
		File file = new File(path,fileName);
		FileWriter fw = new FileWriter(file);
		bw = new BufferedWriter(fw);

		for(String key : branchNames.keySet()) {
			bw.write(key+"," + branchNames.get(key) + "," + branchSales.get(key));
			bw.newLine();
		}
	}catch(IOException e) {
		System.out.println(UNKNOWN_ERROR);
		return false;
	} finally {
		// ファイルを開いている場合
		if(bw != null) {
			try {
				// ファイルを閉じる
				bw.close();
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			}
		}
	}

	return true;
}

}
